export function measure(target: Object, propertyKey: string, descriptor: PropertyDescriptor) {
  const originalMethod = descriptor.value;

  descriptor.value = function (...args: any[]) {
    const start = new Date().getTime();
    const result = originalMethod.apply(this, args);
    const finish = new Date().getTime();
    console.log(`Start time: ${start}`);
    console.log(`Finish time: ${finish}`);
    console.log(`Execution time: ${finish - start} milliseconds`);
    return result;
  }
  return descriptor;
}


export function args(target: Object, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]) {
      const result = originalMethod.apply(this, args);
      console.log(`Begin function with params ${args}`);
      console.log(`Return result : ${result}`);
      return result;
    };
    
    return descriptor;
  }