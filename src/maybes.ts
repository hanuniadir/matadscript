const videoOrNull = () : Video | null => {
    let option : number = Math.floor(Math.random() * 2)
    return option ? 
        { name: "videoOrNull", url: "youtube.com/video", desc: "description", date: (new Date) }
        : null
}

let array : (Video | null)[] = [videoOrNull(),videoOrNull(),videoOrNull()];

let mappedArray = array.map(obj => {
    return (obj?.desc) ? obj.desc : ""
})

console.log(mappedArray);

// string[] because desc is string and also "" so anyway is array of strings...

