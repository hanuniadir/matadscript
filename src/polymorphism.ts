const { measure, args } = require("./decorators")

interface Video {
    name: string;
    url: string;
    desc?: string;
    date: Date;
}

type ReqDesc<T> = {
    [P in keyof T]-?: T[P];
};

type VideoDesc = ReqDesc<Video>;

let obj1: Video = { name: "video1", url: "youtube.com/111", date: (new Date) };
let obj2: Video = { name: "video2", url: "youtube.com/222", desc: "desc2", date: (new Date) };
let obj3: Video = { name: "video3", url: "youtube.com/333", desc: "desc3", date: (new Date) };
let obj4: VideoDesc = { name: "video4", url: "youtube.com/444", desc: "desc4", date: (new Date) };
let obj5: VideoDesc = { name: "video5", url: "youtube.com/555", desc: "desc5", date: (new Date) };
let obj6: VideoDesc = { name: "video6", url: "youtube.com/666", desc: "desc6", date: (new Date) };

class VideoLibrary<T extends Video | VideoDesc> {
    library: T[] = [];
    
    constructor(list: T[]) {
        this.library = list
    }
    
    @measure
    public addToLibrary(video: T) : VideoLibrary<T> | ErrorVideoLibrary<T> {
        let newLib = this.library
        let index = newLib.findIndex(a => a.name === video.name);
        if (index == -1) {
            newLib.push(video);
            return new VideoLibrary(newLib)
        }
        return new ErrorVideoLibrary([video])
    }
    
    deleteFromLibrary = (videoName: string) => {
        let newLib = this.library
        
        let index = newLib.findIndex(a => a.name === videoName);
        if (index != -1) {
            newLib.splice(index, 1);
        }
        return new VideoLibrary(newLib)
    }
    
    @args
    public getPlayList() : string[] {
        let urlist : string[] = []
        this.library.map(video => urlist.push(video.url))
        
        return urlist
    }
}

class ErrorVideoLibrary<T extends Video | VideoDesc> {
    library: T[] = [];
    
    constructor(list: T[]) {
        this.library = list
    }
    getPlayList = () => { return ""}
}

let vL1 = new VideoLibrary([obj1])
let vL2 = new VideoLibrary([obj4])

let vL11 = vL1.addToLibrary(obj2) as VideoLibrary<Video>
let vL111 = vL11.addToLibrary(obj3) as VideoLibrary<Video>
let vL22 = vL2.addToLibrary(obj5) as VideoLibrary<VideoDesc>
let vL222 = vL22.addToLibrary(obj6) as VideoLibrary<VideoDesc>
console.log(vL111.getPlayList())
console.log(vL222.getPlayList())

let libraryArray : (VideoLibrary<Video> | VideoLibrary<VideoDesc>)[] = [vL111, vL222]
let obj10: VideoDesc = { name: "video10", url: "youtube.com/101010", desc: "desc10", date: (new Date) };

let newlib : String[][] = []
libraryArray.map(lib => {
    lib.addToLibrary(obj10)
    newlib.push(lib.getPlayList())
})

console.log(newlib.toString())