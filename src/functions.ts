let stringOrNull: string | null

let one = (str: string) => {
    var lenght = str.length
    if (lenght % 2 == 0) {
        stringOrNull = str.substring(0,(lenght/2));
    } else {
        stringOrNull = null;
    }

    return stringOrNull;
}

console.log("-------- 2 ----------");

console.log(one("WooHoo"));
console.log(one("willNotPrinted!"));


let two = (obj : typeof stringOrNull) => {
    if (typeof obj == "string") {
        return obj.concat(obj);
    }
    return ""
}

console.log("-------- 4 ----------");
console.log("two:" + two("WooHoo"));
console.log("two:" + two(null));

let func5 = (obj : typeof stringOrNull[]) : typeof stringOrNull => {
    if (obj.length > 0) {
        return obj[0]
    }
    return null
}

console.log("-------- 5 ----------");
console.log("func5:" + func5(["one", "two", "tree"]));
console.log("func5:" + func5([]));

let func6 = (obj : any) : obj is typeof stringOrNull => {
    return (typeof obj === stringOrNull)
}
