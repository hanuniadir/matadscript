
// let str1: string = "hello";
let str1 = "hello";
let str2 = "world";

// let num1: number = 1;
let num1 = 1;
let num2 = 2;

console.log(str1+num1);
console.log(str1+num2);
console.log(str2+num1);
console.log(str2+num2);


let arrayNumber = [1,2,3,4,5,6,7,8,9,10]

console.log(arrayNumber.map(num=> {return ("hello"+num)}));

//ההבדל זה בין אונייקט של סטרינג לבין סוג המשתנה type
console.log("hellp".charAt(1));
// מה שיקרה זה ייקח את האיבר במקום 1 במערך איברי התווים במערך התווים שנתנו לו

// any can be any type that you want
// unknown can be any or unknown but not a specific type like boolean

// void return like undefind but we are not use it
// never will not return even the undefind
